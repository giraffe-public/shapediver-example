import { create, ShapeDiverRequestCustomization } from '@shapediver/sdk.geometry-api-sdk-v2';


function uuidv4() {
  return '10000000-1000-4000-8000-100000000000'.replace(/[018]/g, c =>
    // @ts-ignore
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  );
}


export async function transform(fc, projectOrigin: [number, number], params: {ticket: string, url: string}) {
  // TODO is this needed?
  fc.features.forEach(f => {
    // @ts-ignore
    f._projected = f.geometry.coordinates;
  });
  // Initialize the SDK by providing the base URL
  const sdk = create(params.url);

  // This initializes a new session and returns your ShapeDiver Model and your session
  // Please see above on how to obtain a ticket
  const res = await sdk.session.init(params.ticket);

  const txtInput = Object.values(res.parameters).find(o => o.name === 'Text (up to 10k chars)');
  const customizationBody: ShapeDiverRequestCustomization = {
    [txtInput.id]: JSON.stringify(fc)
  };
  console.log('customizationBody', customizationBody);
  const customizationResult = await sdk.utils.submitAndWaitForCustomization(sdk, res.sessionId, customizationBody);
  console.log('customizationResult', customizationResult);

  const txtOutput = Object.values(customizationResult.outputs).find(o => o.name === 'Geojson text output');

  const outFC = {
    type: 'FeatureCollection',
    features: []
  }

  if (txtOutput.content.length > 0) {
    // there is some geojson export
    // @ts-ignore
    outFC.features.push(...JSON.parse(txtOutput.content[0].data).features);
    outFC.features.forEach(f => {
      // need a unique ID
      f.properties.id = uuidv4();

      // TODO nesting seems wrong - just ditch _projected and use coordinates
      f.geometry.coordinates = f._projected;
      delete f._projected;
    });
  }

  // add GLTF output too

  // gltf could be added to a section using coordinates, projectOrigin
  const sdOutput = Object.values(customizationResult.outputs).find(o => o.name === 'SDOutput');
  // @ts-ignore
  const gltf = sdOutput.content.find(o => o.contentType === 'model/gltf-binary');

  outFC.features.push({
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: projectOrigin
    },
    properties: {
      id: uuidv4(),
      gltf: gltf.href,
      scale: 1,
      rotation: 0
    }
  });
  return outFC;

  // silly example just move the shape
  // fc.features.forEach((f) => {
  //   if (f.geometry.type === "Polygon") {
  //     f.geometry.coordinates[0] = f.geometry.coordinates[0].map((c) => [
  //       c[0],
  //       c[1] + 100,
  //     ]);
  //   }
  // });
  // return fc;
}
